# `@scorestats/stylelint-config`

> Developement [Stylelint](https://stylelint.io) config.

## Usage

**Install**:

```bash
$  npm install --save-dev @scorestats/stylelint-config
```

**Edit `package.json`**:

```jsonc
{
  // ...
  "prettier": "@scorestats/stylelint-config"
}
```

**or in project root create file `.stylelintrc`**

```jsonc
{
  "extends": "@scorestats/stylelint-config"
}
```
